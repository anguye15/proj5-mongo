"""
Replacement for RUSA ACP brevet time calculator
(see https://rusa.org/octime_acp.html)

"""

import flask
from flask import Flask, redirect, url_for, request, render_template
import arrow  # Replacement for datetime, based on moment.js
import acp_times  # Brevet time calculations
import config
import logging
from pymongo import MongoClient
import os

###
# Globals
###
app = flask.Flask(__name__)
CONFIG = config.configuration()
app.secret_key = CONFIG.SECRET_KEY
client = MongoClient(os.environ['DB_PORT_27017_TCP_ADDR'], 27017)
db = client.tododb

###
# Pages
###


@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return flask.render_template('calc.html')


@app.errorhandler(404)
def page_not_found(error):
    app.logger.debug("Page not found")
    flask.session['linkback'] = flask.url_for("index")
    return flask.render_template('404.html'), 404


###############
#
# AJAX request handlers
#   These return JSON, rather than rendering pages.
#
###############
@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    begin_date = request.args.get('begin_date')
    begin_time = request.args.get('begin_time')
    distance = request.args.get('distance', type=float)

    app.logger.debug("km={}".format(km))
    app.logger.debug("begin_date={}".format(begin_date))
    app.logger.debug("begin_time={}".format(begin_time))
    app.logger.debug("request.args: {}".format(request.args))
    app.logger.debug("distance={}".format(distance))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km

    percentage = km/distance

    if percentage > 1.2:
        message = "Control location is more than 20 percent over the selected distance. Please change control location according to rules."
    else:
        message = ""

    begin_date_time_string = begin_date + " " + begin_time + ":00"
    begin_date_time_arrow = arrow.get(begin_date_time_string, 'YYYY-MM-DD HH:mm:ss')
    begin_date_time_iso = begin_date_time_arrow.isoformat()
    app.logger.debug("iso={}".format(begin_date_time_iso))

    open_time = acp_times.open_time(km, distance, begin_date_time_iso)
    close_time = acp_times.close_time(km, distance, begin_date_time_iso)
    app.logger.debug("open: " + open_time)
    app.logger.debug("close: " + close_time)
    result = {"open": open_time, "close": close_time, "message": message}
    return flask.jsonify(result=result)

@app.route("/_submit", methods=['POST'])
def submit():
    list_miles = []
    list_km = []
    list_location = []
    list_open = []
    list_close = []

    counter = 0
    for key in request.form.getlist('miles'):
        if key != "":
            list_miles.append(key)
            counter += 1

    for key in request.form.getlist('km'):
        if key != "":
            list_km.append(key)

    for key in request.form.getlist('location'):
        list_location.append(key)

    for key in request.form.getlist('open'):
        if key != "":
            list_open.append(key)

    for key in request.form.getlist('close'):
        if key != "":
            list_close.append(key)

    for i in range(counter):
        item_doc = {
            'miles': list_miles[i],
            'km': list_km[i],
            'location' : list_location[i],
            'open': list_open[i],
            'close': list_close[i]
        }
        db.tododb.insert_one(item_doc)

    return redirect(url_for('index'))

@app.route("/_display")
def display():
    _items = db.tododb.find()
    items = [item for item in _items]

    return render_template('times.html', items=items)

#############

app.debug = CONFIG.DEBUG
if app.debug:
    app.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    print("Opening for global access on port {}".format(CONFIG.PORT))
    app.run(port=CONFIG.PORT, host="0.0.0.0")
